# Read system health check
path "sys/health" {
  capabilities = ["read", "sudo"]
}

# Create and manage ACL policies broadly across Vault

# List existing policies
path "sys/policies/acl" {
  capabilities = ["list"]
}

# Create and manage ACL policies
path "sys/policies/acl/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage authentication methods broadly across Vault

# Manage auth methods broadly across Vault
path "auth/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create, update, and delete auth methods
path "sys/auth/*" {
  capabilities = ["create", "update", "delete", "sudo"]
}

# List auth methods
path "sys/auth" {
  capabilities = ["read"]
}

# Enable and manage the key/value secrets engine at `secret/` path

# List, create, update, and delete key/value secrets
path "secret/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage secrets engines
path "sys/mounts/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# List existing secrets engines.
path "sys/mounts" {
  capabilities = ["read"]
}
# Note: previous policies are snippet from https://developer.hashicorp.com/vault/tutorials/policies/policies

# Enable and manage audit methods broadly across Vault

# Manage audit methods broadly across Vault
path "audit/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Create, update, and delete audit methods
path "sys/audit/*" {
  capabilities = ["create", "update", "delete", "sudo"]
}

# List audit methods
path "sys/audit" {
  capabilities = ["read"]
}

# Manage identity broadly across Vault

# Create and manage entities and groups
path "identity/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Revoke leases
path "sys/leases/revoke/*" {
  capabilities = ["update"]
}

# Enable and manage Azure authentication broadly across Vault

# Manage Azure configuration
path "azure/config" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage Azure roles
path "azure/roles/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Read Azure credentials
path "azure/creds/*" {
  capabilities = ["read"]
}

# Enable and manage the key/value secrets engine at `serviceprincipal/` path

# List, create, update, and delete key/value secrets
path "serviceprincipal/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage GCP authentication broadly across Vault

# Manage GCP configuration
path "gcp/config" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage GCP role set
path "gcp/roleset/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Manage GCP static account
path "gcp/static-account/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage the key/value secrets engine at `serviceaccount/` path

# List, create, update, and delete key/value secrets
path "serviceaccount/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage the key/value secrets engine at `infra/` path

# List, create, update, and delete key/value secrets
path "infra/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage the key/value secrets engine at `kubernetes/` path

# List, create, update, and delete key/value secrets
path "kubernetes/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage the key/value secrets engine at `sslcertificate/` path

# List, create, update, and delete key/value secrets
path "tlscertificate/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}

# Enable and manage the key/value secrets engine at `vm/` path

# List, create, update, and delete key/value secrets
path "vm/*" {
  capabilities = ["create", "read", "update", "delete", "list", "sudo"]
}
