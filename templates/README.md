# Templates

List of template files

## Usage

Example commands

```bash
# Generate environment variables file
kubectl config view --context=$(kubectl config current-context) --raw --output="go-template-file=kubeconfig.envrc.gotemplate"

# Generate Terraform variables file
kubectl config view --context=$(kubectl config current-context) --raw --output="go-template-file=kubeconfig.tfvars.gotemplate"
```
