resource "kubernetes_namespace" "argocd" {
  lifecycle {
    ignore_changes = [metadata]
  }

  metadata {
    name = "argocd"
  }
}
