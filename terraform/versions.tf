terraform {
  required_providers {
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = "~> 4"
    }
    helm = {
      source  = "hashicorp/helm"
      version = ">= 2.9.0, < 3.0.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">= 2.20.0, < 3.0.0"
    }
    powerdns = {
      source  = "pan-net/powerdns"
      version = ">= 1.5.0, < 2.0.0"
    }
    time = {
      source  = "hashicorp/time"
      version = ">= 0.9.1, < 1.0.0"
    }
    vault = {
      source  = "hashicorp/vault"
      version = ">= 3.15.2, < 4.0.0"
    }
  }
  required_version = ">= 1.3.2, < 2.0.0"
}
