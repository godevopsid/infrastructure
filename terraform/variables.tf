variable "mounts" {
  type = map(map(object({
    description               = string
    default_lease_ttl_seconds = number
    max_lease_ttl_seconds     = number
    options                   = map(any)
  })))
  default     = null
  description = "A map of Vault secret mounts"
}

variable "kubernetes_namespaces" {
  type        = list(string)
  default     = null
  description = "List of Kubernetes namespaces"
}

variable "kubernetes_roles" {
  type = map(object({
    namespaces       = list(string)
    policies         = list(string)
    service_accounts = list(string)
  }))
  description = "Role names to kubernetes role config"
}

# Variables for the providers

variable "cf_api_token" {
  type        = string
  sensitive   = true
  description = "The API token for cloudflare provider."
}

variable "cf_zone_id" {
  type        = string
  default     = null
  sensitive   = true
  description = "The Cloudflare zone ID."
}

variable "cf_account_id" {
  type        = string
  default     = null
  sensitive   = true
  description = "The Cloudflare account ID."
}

variable "k8s_host" {
  type        = string
  description = "The hostname (in form of URI) of the Kubernetes API."
}

variable "k8s_cluster_ca_certificate" {
  type        = string
  description = "PEM-encoded root certificates bundle for TLS authentication."
}

variable "k8s_client_certificate" {
  type        = string
  description = "PEM-encoded client certificate for TLS authentication."
}

variable "k8s_client_key" {
  type        = string
  sensitive   = true
  description = "PEM-encoded client certificate key for TLS authentication."
}

variable "vault_address" {
  type        = string
  description = "The hostname (in form of URI) of the Vault endpoint."
}

# variable "vault_username" {
#   type        = string
#   description = "The username of the Vault admin"
# }

# variable "vault_password" {
#   type        = string
#   description = "The password of the Vault admin"
# }
