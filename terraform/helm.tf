# Need to wait a few seconds when removing the argocd resource to give helm
# time to finish cleaning up.
#
# Otherwise, after `terraform destroy`:
# │ Error: uninstallation completed with 1 error(s): uninstall: Failed to purge
#   the release: release: not found

resource "time_sleep" "wait_30_seconds" {
  depends_on = [kubernetes_namespace.argocd]

  destroy_duration = "30s"
}

resource "helm_release" "argocd" {
  name       = "argocd"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argo-cd"
  namespace  = "argocd"
  version    = "7.3.1"

  # Use custom values file after installing the Argo CD chart with default
  # values first because we need to ensure the CRD already exists before
  # creating Argo CD application
  values = [
    file("./helm/argocd/values.yaml"),
    file("./helm/argocd/values-dev.yaml"),
  ]

  depends_on = [time_sleep.wait_30_seconds]
}

resource "helm_release" "argocd_apps" {
  name       = "argocd-apps"
  repository = "https://argoproj.github.io/argo-helm"
  chart      = "argocd-apps"
  namespace  = "argocd"
  version    = "2.0.0"

  values = [
    file("./helm/argocd-apps/values.yaml"),
    file("./helm/argocd-apps/values-dev.yaml"),
  ]

  depends_on = [helm_release.argocd]
}
