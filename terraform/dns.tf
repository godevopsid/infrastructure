resource "cloudflare_record" "www" {
  zone_id = var.cf_zone_id
  name    = "www-tf"
  value   = "203.0.113.10"
  type    = "A"
  proxied = true
}
