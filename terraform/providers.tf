provider "cloudflare" {
  api_token = var.cf_api_token
}

provider "helm" {
  kubernetes {
    host = var.k8s_host

    client_certificate     = base64decode(var.k8s_client_certificate)
    client_key             = base64decode(var.k8s_client_key)
    cluster_ca_certificate = base64decode(var.k8s_cluster_ca_certificate)
  }
}

provider "kubernetes" {
  host = var.k8s_host

  client_certificate     = base64decode(var.k8s_client_certificate)
  client_key             = base64decode(var.k8s_client_key)
  cluster_ca_certificate = base64decode(var.k8s_cluster_ca_certificate)
}

provider "vault" {
  address = var.vault_address
  # auth_login {
  #   path = "auth/userpass/login/${var.vault_username}"
  #
  #   parameters = {
  #     password = var.vault_password
  #   }
  # }
}

