# Vault tfvars
mounts = {
  kv_v2 = {
    kubernetes = {
      description               = "Store key-value secrets related to Kubernetes deployments"
      default_lease_ttl_seconds = 3600
      max_lease_ttl_seconds     = 86400
      options                   = {}
    }
    tlscertificate = {
      description               = "Store TLS certificates provisioned using cert-manager"
      default_lease_ttl_seconds = 3600
      max_lease_ttl_seconds     = 86400
      options                   = {}
    }
  }
}

kubernetes_namespaces = [
  "cert-manager",
  "common",
  "demo",
  "external-dns",
  "kube-prometheus-stack",
]

kubernetes_roles = {
  k8s-cert-manager-reader = {
    namespaces = [
      "common",
      "cert-manager",
    ],
    service_accounts = [
      "external-secrets",
    ]
    policies = [
      "k8s-cert-manager-reader",
    ]
  }
  k8s-demo-reader = {
    namespaces = [
      "common",
      "demo",
    ],
    service_accounts = [
      "external-secrets",
    ]
    policies = [
      "k8s-demo-reader",
    ]
  }
  k8s-external-dns-reader = {
    namespaces = [
      "common",
      "external-dns",
    ],
    service_accounts = [
      "external-secrets",
    ]
    policies = [
      "k8s-external-dns-reader",
    ]
  }
  k8s-kube-prometheus-stack-reader = {
    namespaces = [
      "common",
      "kube-prometheus-stack",
    ],
    service_accounts = [
      "external-secrets",
    ]
    policies = [
      "k8s-kube-prometheus-stack-reader",
    ]
  }
}
