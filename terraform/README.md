# Terraform

Terraform configuration files GodevopsID environment

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3.2, < 2.0.0 |
| <a name="requirement_helm"></a> [helm](#requirement\_helm) | >= 2.9.0, < 3.0.0 |
| <a name="requirement_kubernetes"></a> [kubernetes](#requirement\_kubernetes) | >= 2.20.0, < 3.0.0 |
| <a name="requirement_powerdns"></a> [powerdns](#requirement\_powerdns) | >= 1.5.0, < 2.0.0 |
| <a name="requirement_time"></a> [time](#requirement\_time) | >= 0.9.1, < 1.0.0 |
| <a name="requirement_vault"></a> [vault](#requirement\_vault) | >= 3.15.2, < 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_helm"></a> [helm](#provider\_helm) | 2.9.0 |
| <a name="provider_kubernetes"></a> [kubernetes](#provider\_kubernetes) | 2.20.0 |
| <a name="provider_powerdns"></a> [powerdns](#provider\_powerdns) | 1.5.0 |
| <a name="provider_time"></a> [time](#provider\_time) | 0.9.1 |
| <a name="provider_vault"></a> [vault](#provider\_vault) | 3.15.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [helm_release.argocd](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [helm_release.argocd_apps](https://registry.terraform.io/providers/hashicorp/helm/latest/docs/resources/release) | resource |
| [kubernetes_namespace.argocd](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/namespace) | resource |
| [powerdns_zone.tailsk_id](https://registry.terraform.io/providers/pan-net/powerdns/latest/docs/resources/zone) | resource |
| [time_sleep.wait_30_seconds](https://registry.terraform.io/providers/hashicorp/time/latest/docs/resources/sleep) | resource |
| [vault_auth_backend.kubernetes](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/auth_backend) | resource |
| [vault_auth_backend.userpass](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/auth_backend) | resource |
| [vault_kubernetes_auth_backend_config.kubernetes](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_auth_backend_config) | resource |
| [vault_kubernetes_auth_backend_role.kubernetes](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/kubernetes_auth_backend_role) | resource |
| [vault_mount.kv_v2](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/mount) | resource |
| [vault_policy.admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_admin](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |
| [vault_policy.kubernetes_reader](https://registry.terraform.io/providers/hashicorp/vault/latest/docs/resources/policy) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_k8s_cluster_ca_certificate"></a> [k8s\_cluster\_ca\_certificate](#input\_k8s\_cluster\_ca\_certificate) | PEM-encoded root certificates bundle for TLS authentication. | `string` | n/a | yes |
| <a name="input_k8s_host"></a> [k8s\_host](#input\_k8s\_host) | The hostname (in form of URI) of the Kubernetes API. | `string` | n/a | yes |
| <a name="input_kubernetes_namespaces"></a> [kubernetes\_namespaces](#input\_kubernetes\_namespaces) | List of Kubernetes namespaces | `list(string)` | `[]` | no |
| <a name="input_kubernetes_roles"></a> [kubernetes\_roles](#input\_kubernetes\_roles) | Role names to kubernetes role config | <pre>map(object({<br>    namespaces       = list(string)<br>    policies         = list(string)<br>    service_accounts = list(string)<br>  }))</pre> | n/a | yes |
| <a name="input_mounts"></a> [mounts](#input\_mounts) | A map of Vault secret mounts | <pre>map(map(object({<br>    description               = string<br>    default_lease_ttl_seconds = number<br>    max_lease_ttl_seconds     = number<br>    options                   = map(any)<br>  })))</pre> | n/a | yes |
| <a name="input_pdns_api_key"></a> [pdns\_api\_key](#input\_pdns\_api\_key) | The PowerDNS API key | `string` | n/a | yes |
| <a name="input_pdns_server_url"></a> [pdns\_server\_url](#input\_pdns\_server\_url) | The PowerDNS Server URL | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->
