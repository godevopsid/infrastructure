# Vault `admin` policy
resource "vault_policy" "admin" {
  name   = "admin"
  policy = file("../files/admin-policy.hcl")
}

# Vault secret mounts
resource "vault_mount" "kv_v2" {
  for_each = var.mounts.kv_v2

  type                      = "kv-v2"
  path                      = each.key
  description               = each.value.description
  default_lease_ttl_seconds = each.value.default_lease_ttl_seconds
  max_lease_ttl_seconds     = each.value.max_lease_ttl_seconds
  options                   = each.value.options
}

# Vault policies for `kubernetes/` secret mount
resource "vault_policy" "kubernetes_reader" {
  for_each = toset(var.kubernetes_namespaces)

  name   = "k8s-${each.value}-reader"
  policy = <<-EOT
    path "kubernetes/data/${each.value}/*" {
      capabilities = ["read"]
    }

    path "kubernetes/metadata/${each.value}/*" {
      capabilities = ["list", "read"]
    }
  EOT
}

resource "vault_policy" "kubernetes_admin" {
  for_each = toset(var.kubernetes_namespaces)

  name   = "k8s-${each.value}-admin"
  policy = <<-EOT
    path "kubernetes/data/${each.value}/*" {
      capabilities = ["create", "read", "update", "delete", "list"]
    }

    path "kubernetes/metadata/${each.value}/*" {
      capabilities = ["list", "read", "update", "delete"]
    }
  EOT
}

# Vault authentication backends
resource "vault_auth_backend" "userpass" {
  type        = "userpass"
  description = "username and password based credentials"

  tune {
    default_lease_ttl = "8h"
    max_lease_ttl     = "720h"
  }
}

resource "vault_auth_backend" "kubernetes" {
  type = "kubernetes"

  tune {
    default_lease_ttl = "1h"
    max_lease_ttl     = "24h"
  }
}

resource "vault_kubernetes_auth_backend_config" "kubernetes" {
  backend            = vault_auth_backend.kubernetes.path
  kubernetes_host    = var.k8s_host
  kubernetes_ca_cert = base64decode(var.k8s_cluster_ca_certificate)
  issuer             = var.k8s_host

  # Recommended by Vault documentation
  # https://www.vaultproject.io/docs/auth/kubernetes
  disable_iss_validation = "true"
}

# Vault authentication backend roles
resource "vault_kubernetes_auth_backend_role" "kubernetes" {
  for_each = var.kubernetes_roles

  role_name                        = each.key
  backend                          = vault_auth_backend.kubernetes.path
  bound_service_account_names      = each.value.service_accounts
  bound_service_account_namespaces = each.value.namespaces
  token_policies                   = each.value.policies
  token_ttl                        = 86400


  depends_on = [
    vault_policy.kubernetes_reader,
    vault_policy.kubernetes_admin,
  ]
}
